import React, { Component }from 'react';
import ItemList from '../../shared/components/item-list/item-list';
import Details from '../../shared/components/details/details';
import ErrorBoundry from '../../shared/components/error-boundry/error-boundry';
import SwapiService from '../../shared/api/service';
import Record from '../../shared/components/record/record';
import withData from '../../shared/components/hoc-helpers/with-data';
import withDetails from '../../shared/components/hoc-helpers/with-details';

class Persons extends Component {
  constructor() {
    super();
    this.state = {
      personId: null
    }

    this.ItemListWithData = withData(
      ItemList, {
        getData: this.getPersonsData
    });
    this.DetailsWithData = withDetails(
      Details, {
        getData: this.getSelectedPersonData,
        getImage: this.getSelectedPersonImage
    });
  }

  onItemClicked = (id) => {
    this.setState({ personId: id });
  }

  getPersonsData = () => {
    return SwapiService.getAllPeople();
  }

  getSelectedPersonData = () => {
    return SwapiService.getPerson(this.state.personId);
  }

  getSelectedPersonImage = () => {
    return SwapiService.getPersonImage(this.state.personId);
  }

  render() {
    const { personId } = this.state;
    const { ItemListWithData, DetailsWithData } = this;
    const render = ({ name, gender, birthYear }) => (
      <>
        <span>{ name }</span>
        <span>{ `${ gender } ${ birthYear }` }</span>
      </>
    );

    return (
      <ErrorBoundry>
        <main className="two-col-container">
          <section className="box-container">
            <ItemListWithData
              id={ personId }
              onItemClicked={ this.onItemClicked }
            >
              { render }
            </ItemListWithData>
          </section>

          <section className="box-container">
            <DetailsWithData
              id={ personId }
              entity={ 'person' }
            >
              <Record field="gender" label="Gender" />
              <Record field="eyeColor" label="Eye color" />
              <Record field="birthYear" label="Birth year" />
            </DetailsWithData>
          </section>
        </main>
      </ErrorBoundry>
    );
  }
}

export default Persons;
