import React, { Component }from 'react';
import ItemList from '../../shared/components/item-list/item-list';
import Details from '../../shared/components/details/details';
import SwapiService from '../../shared/api/service';
import ErrorBoundry from '../../shared/components/error-boundry/error-boundry';
import Record from '../../shared/components/record/record';
import withData from '../../shared/components/hoc-helpers/with-data';
import withDetails from '../../shared/components/hoc-helpers/with-details';

class Planets extends Component {
  constructor() {
    super();
    this.state = {
      planetId: null
    }

    this.ItemListWithData = withData(
      ItemList, {
        getData: this.getPlanetsData
    });
    this.DetailsWithData = withDetails(
      Details, {
        getData: this.getSelectedPlanetData,
        getImage: this.getSelectedPlanetImage
    });
  }

  onItemClicked = (id) => {
    this.setState({ planetId: id });
  }

  getPlanetsData = () => {
    return SwapiService.getAllPlanets();
  }

  getSelectedPlanetData = () => {
    return SwapiService.getPlanet(this.state.planetId);
  }

  getSelectedPlanetImage = () => {
    return SwapiService.getPlanetImage(this.state.planetId);
  }

  render() {
    const { planetId } = this.state;
    const { ItemListWithData, DetailsWithData } = this;
    const render = ({ name, diameter }) => (
      <>
        <span>{ name }</span>
        <span>{ `${ diameter }` }</span>
      </>
    );

    return (
      <ErrorBoundry>
        <main className="two-col-container">
          <section className="box-container">
            <ItemListWithData
              id={ planetId }
              onItemClicked={ this.onItemClicked }
            >
              { render }
            </ItemListWithData>
          </section>

          <section className="box-container">
            <DetailsWithData
              id={ planetId }
              entity="planet"
            >
              <Record field="population" label="Population" />
              <Record field="rotation" label="Rotation" />
              <Record field="diameter" label="Diameter" />
            </DetailsWithData>
          </section>
        </main>
      </ErrorBoundry>
    )
  }
}

export default Planets;
