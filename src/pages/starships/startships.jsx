import React, { Component }from 'react';
import ItemList from '../../shared/components/item-list/item-list';
import Details from '../../shared/components/details/details';
import SwapiService from '../../shared/api/service';
import ErrorBoundry from '../../shared/components/error-boundry/error-boundry';
import Record from '../../shared/components/record/record';
import withData from '../../shared/components/hoc-helpers/with-data';
import withDetails from '../../shared/components/hoc-helpers/with-details';

class Starships extends Component {
  constructor() {
    super();
    this.state = {
      starshipId: null
    }

    this.ItemListWithData = withData(
      ItemList, {
        getData: this.getStarshipsData
    });
    this.DetailsWithData = withDetails(
      Details, {
        getData: this.getSelectedStarshipData,
        getImage: this.getSelectedStarshipImage
    });
  }

  onItemClicked = (id) => {
    this.setState({ starshipId: id });
  }

  getStarshipsData = () => {
    return SwapiService.getAllStarships();
  }

  getSelectedStarshipData = () => {
    return SwapiService.getStarship(this.state.starshipId);
  }

  getSelectedStarshipImage = () => {
    return SwapiService.getStarshipImage(this.state.starshipId);
  }

  render() {
    const { starshipId } = this.state;
    const { ItemListWithData, DetailsWithData } = this;
    const render = ({ name, model, costInCredits }) => (
      <>
        <span>{ name }</span>
        <span>{ `${ model } $${ costInCredits }` }</span>
      </>
    );

    return (
      <ErrorBoundry>
        <main className="two-col-container">
          <section className="box-container">
            <ItemListWithData
              id={ starshipId }
              onItemClicked={ this.onItemClicked }
            >
              { render }
            </ItemListWithData>
          </section>

          <section className="box-container">
            <DetailsWithData
              id={ starshipId }
              entity="starship"
            >
              <Record field="model" label="Model" />
              <Record field="manufacturer" label="Manufacturer" />
              <Record field="costInCredits" label="Cost in credits" />
              <Record field="length" label="Length" />
              <Record field="crew" label="Crew" />
              <Record field="passengers" label="Passengers" />
              <Record field="cargoCapacity" label="Cargo capacity" />
            </DetailsWithData>
          </section>
        </main>
      </ErrorBoundry>
    )
  }
}

export default Starships;
