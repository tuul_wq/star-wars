import React from 'react';
import Planets from '../planets/planets';
import Persons from '../persons/persons';
import Starships from '../starships/startships';

function Home() {
  return (
    <>
      <Planets />
      <Starships />
      <Persons />
    </>
  )
}

export default Home;
