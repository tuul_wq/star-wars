import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import NavBar from '../shared/components/nav-bar/nav-bar';
import RandomPlanet from '../shared/components/random-planet/random-planet';
import Routes from '../routes';
import './app.scss';

function App() {
  return (
    <div id="container">
      <Router>
        <NavBar />
        <RandomPlanet />

        <Routes />
      </Router>
    </div>
  );
}

export default App;
