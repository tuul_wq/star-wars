import React from 'react';

import { Switch, Route } from 'react-router-dom';
import Home from './pages/home/home';
import Startships from './pages/starships/startships';
import Planets from './pages/planets/planets';
import Persons from './pages/persons/persons';

function Routes() {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route path="/planets">
        <Planets />
      </Route>
      <Route path="/persons">
        <Persons />
      </Route>
      <Route path="/starships">
        <Startships />
      </Route>
    </Switch>
  );
}

export default Routes;
