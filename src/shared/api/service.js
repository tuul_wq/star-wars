class SwapiService {
  constructor() {
    this._apiBase = 'https://swapi.dev/api';
    this._imageBase = 'https://starwars-visualguide.com/assets/img';
  }

  async _getResource(url) {
    const res = await fetch(this._apiBase + url);
    if (!res.ok) {
      throw new Error(`Could not fetch ${url}, error status: ${res.status}`);
    }
    return await res.json();
  }

  async getAllPeople() {
    const people = await this._getResource('/people/');
    return people.results.map(this._transformPerson.bind(this));
  }

  async getAllStarships() {
    const starships = await this._getResource('/starships/');
    return starships.results.map(this._transformStarship.bind(this));
  }

  async getAllPlanets() {
    const planets = await this._getResource('/planets/');
    return planets.results.map(this._transformPlanet.bind(this));
  }

  async getPerson(personId) {
    const person = await this._getResource(`/people/${personId}/`);
    return this._transformPerson(person);
  }

  async getStarship(starshipId) {
    const starship = await this._getResource(`/starships/${starshipId}/`);
    return this._transformStarship(starship);
  }

  async getPlanet(planetId) {
    const planet = await this._getResource(`/planets/${planetId}/`);
    return this._transformPlanet(planet);
  }

  getPersonImage(personId) {
    return `${this._imageBase}/characters/${personId}.jpg`;
  }

  getStarshipImage(starshipId) {
    return `${this._imageBase}/starships/${starshipId}.jpg`;
  }

  getPlanetImage(planetId) {
    return `${this._imageBase}/planets/${planetId}.jpg`;
  }

  _transformPerson(person) {
    return {
      id: this._extractId(person.url),
      name: person.name,
      gender: person.gender,
      birthYear: person.birth_year,
      eyeColor: person.eye_color
    }
  }

  _transformStarship(starship) {
    return {
      id: this._extractId(starship.url),
      name: starship.name,
      model: starship.model,
      manufacturer: starship.manufacturer,
      costInCredits: starship.cost_in_credits,
      length: starship.length,
      crew: starship.crew,
      passengers: starship.passengers,
      cargoCapacity: starship.cargo_capacity
    }
  }

  _transformPlanet(planet) {
    return {
      id: this._extractId(planet.url),
      name: planet.name,
      population: planet.population,
      rotation: planet.rotation_period,
      diameter: planet.diameter
    }
  }

  _extractId(string) {
    return string.match(/[\d]{1,2}/)[0];
  }
}

export default new SwapiService();
