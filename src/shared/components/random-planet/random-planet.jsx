import React, { Component } from 'react';
import SwapiService from '../../api/service';
import Spinner from '../spinner/spinner';
import ErrorIndicator from '../error-indicator/error-indicator';
import './random-planet.scss';

class RandomPlanet extends Component {
  constructor() {
    super();
    this.state = {
      planet: {},
      loading: true,
      error: false
    };
  }

  componentDidMount() {
    this.updatePlanet();
    this.interval = setInterval(this.updatePlanet, 3000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  updatePlanet = async () => {
    const randomId = Math.ceil(Math.random() * 20);
    try {
      const planet = await SwapiService.getPlanet(randomId);
      this.setState({ planet, loading: false });
    } catch {
      this.setState({ loading: false, error: true });
    }
  }

  render() {
    const { planet, loading, error } = this.state;

    return (
      <section className="box-container">
        { error ? <ErrorIndicator /> : null }
        { loading ? <Spinner /> : null }
        { !loading && !error ? <PlanetView { ...planet } /> : null }
      </section>
    );
  }
}

function PlanetView({ id, name, population, rotation, diameter }) {
  const imageSrc = `https://starwars-visualguide.com/assets/img/planets/${id}.jpg`;
  return (
    <div className="random-planet">
      <img src={ imageSrc } className="planet-img" alt="Random planet" />
      <div>
        <h2 className="planet-name">{ name }</h2>
        <ul className="planet-info">
          <li>Population { population }</li>
          <li>Rotation period { rotation }</li>
          <li>Diameter { diameter }</li>
        </ul>
      </div>
    </div>
  );
}

export default RandomPlanet;
