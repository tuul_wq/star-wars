import React from 'react';
import './item-list.scss';

function ItemList({ id, data, onItemClicked, children }) {
  const listItemClass = (itemId) => `list-item${ id === itemId ? ' active' : '' }`;

  return (
    <ul className="item-list">
      { data.map(({ id, ...rest }) =>
        <li key={ id } className={ listItemClass(id) } onClick={ () => onItemClicked(id) }>
          { children(rest) }
        </li>
      ) }
    </ul>
  )
}

export default ItemList;
