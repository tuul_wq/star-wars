import React from 'react';
import icon from './death-star.svg';
import './error-indicator.scss'; 

function ErrorIndicator() {
  return (
    <div className="error-indicator">
      <img src={ icon } alt="Error icon"/>
      <div className="error-message">
        <span className="error-boom">Boom!</span>
        <span>something has gone terribly wrong</span>
        <span>(but we already sent droids to fix it)</span>
      </div>
    </div>
  )
}

export default ErrorIndicator;
