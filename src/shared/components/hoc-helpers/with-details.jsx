import React, { Component } from 'react';
import ErrorIndicator from '../error-indicator/error-indicator';
import Spinner from '../spinner/spinner';

function withDetails(View, { getData, getImage }) {
  return class extends Component {
    state = {
      data: null,
      image: null,
      loading: false,
      error: false
    }

    componentDidUpdate(prevProps) {
      if (prevProps.id !== this.props.id) {
        this.downloadData();
      }
    }

    async downloadData() {
      try {
        this.setState({ loading: true });
        const data = await getData();
        this.setState({
          data,
          image: getImage(),
          loading: false
        });
      } catch {
        this.setState({ loading: false, error: true });
      }
    }

    render() {
      const { loading, error, data, image } = this.state;

      if (loading) return <Spinner />;
      if (!loading && error) return <ErrorIndicator />;

      return <View { ...this.props } data={ data } image={ image } />
    }
  }
}

export default withDetails;