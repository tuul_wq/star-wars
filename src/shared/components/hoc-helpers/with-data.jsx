import React, { Component } from 'react';
import ErrorIndicator from '../error-indicator/error-indicator';
import Spinner from '../spinner/spinner';

function withData(View, { getData }) {
  return class extends Component {
    state = {
      data: null,
      loading: true,
      error: false
    }

    componentDidMount() {
      this.downloadData();
    }

    async downloadData() {
      try {
        this.setState({ loading: true });
        const data = await getData();
        this.setState({
          data,
          loading: false
        });
      } catch {
        this.setState({ loading: false, error: true });
      }
    }

    render() {
      const { loading, error, data } = this.state;

      if (loading) return <Spinner />;
      if (!loading && error) return <ErrorIndicator />;

      return <View { ...this.props } data={ data } />
    }
  }
}

export default withData;