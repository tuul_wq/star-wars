import React, { Children, cloneElement } from 'react';
import './details.scss';

function Details({ id, data, image, entity, children }) {
  if (!id || !data) {
    return <span className="details-empty">No { entity } selected</span>;
  }

  return (
    <div className="details">
      <img src={ image } className="details-img" alt="Details"/>
      <div>
        <h2>{ data.name }</h2>
        <ul className="details-info">
          { Children.map(children, (child) => cloneElement(child, { data })) }
        </ul>
      </div>
    </div>
  )
}

export default Details;
