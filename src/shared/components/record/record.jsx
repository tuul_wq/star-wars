import React from 'react';
import './record.scss';

function Record({ data, label, field }) {
  return (
    <li className="record">
      <span className="record-title">{ label }</span>
      <span className="record-value">{ data[field] }</span>
    </li>
  )
}

export default Record;
