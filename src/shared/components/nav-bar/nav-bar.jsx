import React from 'react';
import { NavLink } from 'react-router-dom';
import logo from '../../../shared/assets/images/logo.png';
import './nav-bar.scss';

function NavBar() {
  return (
    <nav id="navigation">
      <NavLink to="/" className="logo">
        <img  className="logo-img" src={logo} alt="Star wars logo"/>
        <span className="logo-name">Star Wars</span>
      </NavLink>
      <ul className="nav-list">
        <li className="nav-item">
          <NavLink to="/planets" activeClassName="nav-item__active">Planets</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/starships" activeClassName="nav-item__active">Starships</NavLink>
        </li>
        <li className="nav-item">
          <NavLink to="/persons" activeClassName="nav-item__active">Persons</NavLink>
        </li>
      </ul>
    </nav>
  );
}

export default NavBar;
